/* eslint-disable no-useless-catch */
import { AUTH } from '@/config/firebase.js'

const auth = {
  namespaced: true,
  state: {
    user: {}
  },
  getters: {
    USER: (state) => state.user
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload
    },
    CLEAR_USER(state) {
      state.user = null
    }
  },
  actions: {
    async LOGIN({ commit }, payload) {
      const { email, password } = payload;
      try {
        const user = await AUTH.signInWithEmailAndPassword(email, password);
        console.log(user);
        commit('SET_USER', user);
        return true;
      } catch (err) {
        throw err
      }
    },
    async LOGOUT({ commit }) {
      await AUTH.signOut();
      commit('CLEAR_USER');
      return true;
    }
  },
}

export default auth;