/* eslint-disable no-unused-vars */
import { DB } from '@/config/firebase.js'

const notes = {
  namespaced: true,
  state: {
    notes: [
      // {
      //   id: '',
      //   title: '',
      //   description: ''
      // }
    ]
  },
  getters: {
    NOTES: (state) => state.notes
  },
  mutations: {
    SET_NOTES(state, payload) {
      state.notes = payload;
    },
    ADD_NOTE(state, payload) {
      state.notes.push(payload)
    },
    UPDATE_NOTE(state, payload) {
      const index = state.notes.findIndex(n => n.id === payload.id);
      if (index >= 0) {
        state.notes.splice(index, 1, payload)
      }
    },
    DELETE_NOTE(state, payload) {
      const index = state.notes.findIndex(n => n.id === payload.id);
      if (index >= 0) {
        state.notes.splice(index, 1)
      }
    }
  },
  actions: {
    async FETCH_ALL({ commit }) {
      const notesRef = await DB.collection('notes').get();
      const notes = notesRef.docs.map(doc => {
        const data = doc.data();
        data.id = doc.id;
        return data;
      });
      commit('SET_NOTES', notes);
    },
    async GET_NOTE(context, id) {
      const noteRef = await DB.collection('notes').doc(id).get();
      if (noteRef.empty) {
        return null;
      }

      const data = noteRef.data();
      data.id = noteRef.id;
      return data;
    },
    async QUERY_NOTE({ commit }) {
      const dateNow = new Date();
      const notesRef = await DB.collection('notes')
        .where('isEditable', '==', true)
        .where('userId', '!=', 'CA')
        .where('date', '<=', dateNow)
        .where('userId', 'in', ['TD', 'SC'])
        .where('userId', 'not-in', ['CA', 'SC'])
        .where('liked', 'array-contains', 'alec')
        .where('liked', 'array-contains-any', ['alec', 'clarissa'])
        .get()
        const notes = notesRef.docs.map(doc => {
          const data = doc.data();
          data.id = doc.id;
          return data;
        });
        commit('SET_NOTES', notes);
    },
    async ADD ({ commit }, payload) {
      const newNote = await DB.collection('notes').add(payload);
      console.log(newNote);

      const addedNote = {
        id: newNote.id,
        ...payload
      }

      commit('ADD_NOTE', addedNote);
      return true;
    },
    async UPDATE({ commit }, payload) {
      const { id, title, description } = payload;
      const noteRef = await DB.collection('notes').doc(id).update({
        title: title,
        description: description
      });
      console.log(noteRef);
      commit('UPDATE_NOTE', payload);
    },
    async DELETE({ commit }, payload) {
      const noteRef = await DB.collection('notes').doc(payload.id).delete();
      console.log(noteRef);
      commit('DELETE_NOTE', payload);
    }
  }
};

export default notes;