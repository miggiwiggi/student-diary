import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth.js';
import notes from './modules/notes.js';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drawer: false,
    message: 'Wawers',
    user: {}
  },
  getters: {
    DRAWER: (state) => state.drawer,
    MESSAGE: (state) => state.message,
    USER: (state) => state.user,
  },
  mutations: {
    TOGGLE_DRAWER(state, payload) {
      state.drawer = payload
    },
    SET_MESSAGE(state, payload) {
      state.message = payload;
    }
  },
  actions: {
    async SAMPLE({ commit }, payload) {
      commit('SET_MESSAGE', payload);
    }
  },
  modules: {
    auth,
    notes
  }
})
