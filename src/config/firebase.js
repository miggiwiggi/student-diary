import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const config = {
  apiKey: "AIzaSyAO7mq0ANQ84i1jSURir-3I3QzBWUN1wJs",
  authDomain: "miggy-student-diary.firebaseapp.com",
  projectId: "miggy-student-diary",
  storageBucket: "miggy-student-diary.appspot.com",
  messagingSenderId: "441043709037",
  appId: "1:441043709037:web:d0027cf47219cc2fe36364"
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
} else {
  firebase.app()
}

export const AUTH = firebase.auth()
export const DB = firebase.firestore()
export const STORAGE = firebase.storage()
export const FIREBASE = firebase