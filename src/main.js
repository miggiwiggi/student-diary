import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import { AUTH } from '@/config/firebase.js'

Vue.config.productionTip = false

let app;
AUTH.onAuthStateChanged((user) => {
  if (user) {
    store.commit('auth/SET_USER', user);
    if (!app) {
      app = new Vue({
        router,
        store,
        vuetify,
        render: h => h(App)
      }).$mount('#app')
    }
    router.push({ name: 'Home' })

  } else {
    if (!app) {
      app = new Vue({
        router,
        store,
        vuetify,
        render: h => h(App)
      }).$mount('#app')
    }
    router.push({ name: 'Login' })
  }
})
